package afifudin.fatakhul.appx0bpendatatanpegawai

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class AdapterPegawai(val dataPeg : List<HashMap<String,String>>, val mainActivity: MainActivity) :
    RecyclerView.Adapter<AdapterPegawai.HolderDataPegawai>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterPegawai.HolderDataPegawai {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_pegawai,p0,false)
        return  HolderDataPegawai(v)
    }

    override fun getItemCount(): Int {
        return dataPeg.size
    }

    override fun onBindViewHolder(p0: AdapterPegawai.HolderDataPegawai, p1: Int) {
        val data = dataPeg.get(p1)
        p0.txtNama.setText(data.get("nama"))
        p0.txtAlamat.setText(data.get("alamat"))
        p0.txtJabatan.setText(data.get("jabatan"))
        p0.txtTelepon.setText(data.get("no_telepon"))
        if(p1.rem(2) == 0) p0.clayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.clayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.clayout.setOnClickListener({
            val pos = mainActivity.dftrJabatan.indexOf(data.get("jabatan"))
            mainActivity.spJabatan.setSelection(pos)
            mainActivity.idPegawai = data.get("id").toString()
            mainActivity.edNama.setText(data.get("nama"))
            mainActivity.edNoTelp.setText(data.get("no_telepon"))
            mainActivity.edAlamat.setText(data.get("alamat"))
            Picasso.get().load(data.get("urlImg")).into(mainActivity.imgUpload)
        })
        if(!data.get("urlImg").equals(""))
            Picasso.get().load(data.get("urlImg")).into(p0.photo)
    }

    class HolderDataPegawai(v: View) : RecyclerView.ViewHolder(v){
        val txtNama = v.findViewById<TextView>(R.id.txtNama)
        val txtJabatan = v.findViewById<TextView>(R.id.txtJabatan)
        val txtAlamat = v.findViewById<TextView>(R.id.txtAlamat)
        val txtTelepon = v.findViewById<TextView>(R.id.txtTelepon)
        val photo = v.findViewById<ImageView>(R.id.imgPegawai)
        val clayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }
}

