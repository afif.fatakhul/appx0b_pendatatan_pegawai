package afifudin.fatakhul.appx0bpendatatanpegawai

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var mediaHelper: MediaHelper
    lateinit var PegAdapter : AdapterPegawai
    lateinit var jabAdapter : ArrayAdapter<String>
    var dftrPegawai = mutableListOf<HashMap<String,String>>()
    var dftrJabatan = mutableListOf<String>()
    val mainUrl = "http://192.168.43.140/Appx0bPegawaiWeb/"
    val url = mainUrl+"show_pegawai.php"
    val url2 = mainUrl+"show_jabatan.php"
    val url3 = mainUrl+"query_pegawai.php"
    var imStr = ""
    var namaFile = ""
    var fileUri = Uri.parse("")
    var pilihJabatan = ""
    var idPegawai = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        PegAdapter = AdapterPegawai(dftrPegawai,this)
        mediaHelper = MediaHelper()
        listPgw.layoutManager = LinearLayoutManager(this)
        listPgw.adapter = PegAdapter
        jabAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            dftrJabatan)
        spJabatan.adapter = jabAdapter
        spJabatan.onItemSelectedListener = itemSelected
        try{
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        }catch (e: Exception){
            e.printStackTrace()
        }
        imgUpload.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDelete -> {
                queryInsertUpdateDelete("delete")
            }
            R.id.btnFind -> {
                showDtPegawai(edNama.text.toString())
            }
            R.id.btnInsert -> {
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate -> {
                queryInsertUpdateDelete("update")
            }
            R.id.imgUpload -> {
                requestPermission()
            }
        }
    }

    fun requestPermission() = runWithPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        fileUri = mediaHelper.getOutputMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
        startActivityForResult(intent,mediaHelper.getRcCamera())
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spJabatan.setSelection(0)
            pilihJabatan = dftrJabatan.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihJabatan = dftrJabatan.get(position)
        }
    }

    override fun onStart() {
        super.onStart()
        getNamaJabatan("")
        showDtPegawai("")
    }

    fun getNamaJabatan(nmJabatan : String){
        val request = object :StringRequest(Request.Method.POST,url2,
            Response.Listener { response ->
                dftrJabatan.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    dftrJabatan.add(jsonObject.getString("jabatan"))
                }
                jabAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error -> }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama",nmJabatan)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDtPegawai(namaPegawai : String){
        val request = object :StringRequest(Request.Method.POST,url,
            Response.Listener { response ->
                dftrPegawai.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var pegawai = HashMap<String,String>()
                    pegawai.put("id",jsonObject.getString("id"))
                    pegawai.put("nama",jsonObject.getString("nama"))
                    pegawai.put("no_telepon",jsonObject.getString("no_telepon"))
                    pegawai.put("alamat",jsonObject.getString("alamat"))
                    pegawai.put("jabatan",jsonObject.getString("jabatan"))
                    pegawai.put("urlImg",jsonObject.getString("urlImg"))
                    dftrPegawai.add(pegawai)
                }
                PegAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama",namaPegawai)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK)
            if(requestCode == mediaHelper.getRcCamera()){
                imStr = mediaHelper.getBitmapToSting(imgUpload,fileUri)
                namaFile = mediaHelper.getMyFileName()
            }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDtPegawai("")
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" -> {
                        hm.put("mode","POST_DATA")
                        hm.put("nama",edNama.text.toString())
                        hm.put("no_telepon",edNoTelp.text.toString())
                        hm.put("alamat",edAlamat.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("jabatan",pilihJabatan)
                    }
                    "delete" -> {
                        hm.put("mode","DELETE_DATA")
                        hm.put("id_pegawai",idPegawai)
                    }
                    "update" -> {
                        hm.put("mode","UPDATE_DATA")
                        hm.put("id_pegawai",idPegawai)
                        hm.put("nama",edNama.text.toString())
                        hm.put("no_telepon",edNoTelp.text.toString())
                        hm.put("alamat",edAlamat.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("jabatan",pilihJabatan)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}
